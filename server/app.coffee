'use strict'

express = require 'express'
mongoose = require 'mongoose'
fs = require 'fs'


# Configure Express
app = express()
# app.use express.bodyParser()
# app.use express.cookieParser()
# app.use express.session(secret: 'madoka')


require('./routes')(app)


mongoose.connect 'mongodb://localhost/tampham'


app.listen 3047
console.log 'Listening on port 3047...'
