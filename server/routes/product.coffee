'use strict'

Product 	= require '../models/product'
User		= require '../models/user'
Guid 		= require '../utility'
bl_user 	= require '../business/bl_user'
bl_product 	= require '../business/bl_product'


getAll = (req, res) ->
	p = new Product
		title: 'chuyện tình dưới mưa, tần khánh'
		style: Guid.createGuid()
		description: 'nếu biết tình như thế'
		images: [
			{kind: 'catalog', url: 'tampham.png'},
			{kind: 'catalog', url: 'tampham.png'}]
		variants: [
			{color: 'red'},
			{color: 'yellow'}]
		categories: [
			{name: 'giao duc'},
			{name: 'kinh te'}]
		catalogs: [
			{name: 'u'},
			{name: 'v'}]

	console.log p
	console.log '---------------'
	console.log p.id

	user = new User
		userName: 'tampham47' + Guid.createGuid()
		email: 'tampham47@live.com' + Guid.createGuid()
	console.log user

	res.send p

	user.save (err)->
		throw err if err
		console.log 'user.save ' + user.id

		p._user = user.id
		p.save (err) ->
			throw err if err
			console.log 'product.save'

			Product
			.findById(p.id)
			.populate('_user')
			.exec (err, product) ->
				console.log 'product find by id'
				console.log 'userName: ' + product._user.userName


testMethod = (req, res) ->
	console.log 'testMethod'
	res.send 'testMethod'

	test 'chuyeen tinh', (a,b)->
		console.log a + b

business = (req, res) ->
	console.log 'business'

	bl_user.getAll (err, status) ->
		console.log 'getAll' + status

	bl_product.getAll (err, product) ->
		console.log 'bl_product.getAll'
		console.log product

	bl_product.getById '', (err, product) ->
		console.log 'bl_product.getById'
		console.log product
		res.send product


# call back function
test = (u, callback) ->
	console.log 'test' + u
	callback(1, 2)

module.exports = (app) ->
	app.get '/', getAll
	app.get '/test', testMethod
	app.get '/business', business
