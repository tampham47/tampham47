'use strict'


# Cross-origin resource sharing
allowCors = (request, response, next) ->
	response.set 'Access-Control-Allow-Origin' , 'http://localhost:8049'
	response.set 'Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE'
	response.set 'Access-Control-Allow-Headers', 'Content-Type'
	response.set 'Access-Control-Allow-Credentials', true

	# response.header 'Access-Control-Allow-Origin', 'http://localhost:8049'
	# response.header 'Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE'
	# response.header 'Access-Control-Allow-Headers', 'Content-Type'

	next()

	# if request.method is 'OPTIONS'
	# 	response.send(200)
	# else
	# 	next()


ensureAuthenticated = (request, response, next) ->
	if request.url is '/auth' or request.url.indexOf('/tam') > 0
		next()
	else if request.url.indexOf('/approval') is 0
		next()
	else if request.isAuthenticated()
		next()
	else
		response.send(401) # Unauthorized


module.exports = (app) ->
	app.all '*', allowCors
	# app.all '*', ensureAuthenticated

	require('./user')(app)
	require('./product')(app)

