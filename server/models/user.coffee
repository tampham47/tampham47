'use strict'

mongooes = require 'mongoose'
Schema = mongooes.Schema

UserSchema = new Schema
	userName:
		type: String
		unique: true

	email:
		type: String
		unique: true

	avatar:
		type: String

	modified:
		type: Date
		default: Date.now

module.exports = mongooes.model 'User', UserSchema
