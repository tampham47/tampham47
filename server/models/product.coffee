'use strict'

mongooes = require 'mongoose'
Schema = mongooes.Schema

Guid = require '../utility'
User = require '../models/user'


Sizes = new Schema
	_id:
		type: String
		default: Guid.createGuid
	size:
		type: String
		required: true

	available:
		type: Number
		required: true
		min: 0
		max: 1000

	sky:
		type: String
		required: true
		validate:
			[/[a-zA-Z0-9]/, 'Product sky should only have letters and numbers']

	price:
		type: Number
		required: true
		min: 0


Images = new Schema
	_id:
		type: String
		default: Guid.createGuid
	kind:
		type: String
		enum: ['thumbnail', 'catalog', 'detail', 'zoom']
		required: true

	url:
		type: String
		required: true


Variants = new Schema
	_id:
		type: String
		default: Guid.createGuid
	color: String
	images: [Images]
	sizes: [Sizes]


Categories = new Schema
	_id:
		type: String
		default: Guid.createGuid
	name: String


Catalogs = new Schema
	_id:
		type: String
		default: Guid.createGuid
	name: String


Product = new Schema
	_id:
		type: String
		default: Guid.createGuid

	_user:
		type: Schema.Types.ObjectId
		ref: 'User'

	title:
		type: String
		required: true

	description:
		type: String
		required: true

	style:
		type: String
		unique: true

	images: [Images]

	categories: [Categories]

	catalogs: [Catalogs]

	variants: [Variants]

	modified:
		type: Date
		default: Date.now


Product.path('title').validate (v) ->
	# console.log 'validate title'
	# console.log v
	return v.length > 10 && v.length < 70

# Product.path('style').validate (v) ->
# 	console.log 'validate style'
# 	console.log v
# 	return v.length < 40
# , 'Product stype attribute is should be less than 40 characters.'

Product.path('description').validate (v) ->
	# console.log 'validate description'
	# console.log v
	return v.length > 10
, 'Product description should be more than 100 characters.'

Product.methods.saveAll = ->
	console.log 'save: completed'


# Product.pre 'save', (next)->
# 	console.log 'pre save'
# 	next()

module.exports = mongooes.model 'Product', Product
# module.exports = mongooes.model 'Sizes', Sizes
# module.exports = mongooes.model 'Images', Images
# module.exports = mongooes.model 'Variants', Variants
# module.exports = mongooes.model 'Categories', Categories
# module.exports = mongooes.model 'Catalogs', Catalogs
