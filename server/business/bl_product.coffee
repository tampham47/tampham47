'use strict'

Product = require '../models/product'

insert = (product, call) ->
	console.log 'insert'
	product.save (err) ->
		call err, product.id

update = (product, call) ->
	console.log 'update'
	product.save (err) ->
		call err, product.id

remove = (id, call) ->
	Product.remove
		_id: id
	, (err) ->
		call err

getById = (id, call) ->
	Product
	.findById(id)
	.populate('_user')
	.exec (err, product) ->
		call err, product

getAll = (call) ->
	console.log 'getAll'
	call null, 'ok'

getByUser = (userId, call) ->
	products = []
	err = 'null'
	call err, products


module.exports = {
	insert, update, remove,
	getById, getAll, getByUser
}
