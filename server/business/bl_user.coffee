'use strict'

User = require '../models/user'

getById = (id, call) ->
	User.findById id, (err, user) ->
		call err, user

getAll = (call) ->
	User.find {}, (err, users) ->
		call err, users

	# console.log 'bluser.getAll'
	# call(null, 'ok')

module.exports = {
	getById,
	getAll
}
