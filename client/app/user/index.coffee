'use strict'

angular.module('tampham')
.factory 'User', ($resource) ->

	actions = angular.extend {}, null, #customActions,
		create: method: 'POST'
		update: method: 'PUT'

	object = $resource 'http://localhost:3047/user', { _id: '@_id' }, actions

	object::$save = ->
		method = if @_id then '$update' else '$create'
		this[method].apply this, arguments

	object::$isNew = ->
		!@_id

	return object


# .factory 'User', ->
# 	get: ->
# 		console.log 'get'
# 	put: ->
# 		console.log 'put'
# 	post: ->
# 		console.log 'post'
