'use strict'

# require order is important

angular.module 'tampham', [
	'ngRoute'
	'ngResource'
	'tampham.templates'
	# 'tampham.user'

	'tampham.dashboard'
	'tampham.account'
	'tampham.product'
]
