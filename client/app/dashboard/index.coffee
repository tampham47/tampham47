'use strict'

angular.module('tampham.dashboard', [])

# config route foreach controller
.config ($routeProvider) ->
	$routeProvider.when '/',
		controller: 'DashboardCtrl'
		templateUrl: 'app/dashboard/index.jade'


.controller 'DashboardCtrl', ($scope, $location) ->
	$scope.controllerName = 'tampham.dashboard'
