'use strict'

product = angular.module('tampham.product', [])

# config route foreach controller
product.config ($routeProvider) ->
	$routeProvider.when '/product',
		controller: 'ProductCtrl'
		templateUrl: 'app/product/index.jade'

product.controller 'ProductCtrl', ($scope, $location) ->
	# User.get()
	# User.put()
		# userName: 'tampham'
		# email: 'tampham47@live.com'

	$scope.controllerName = 'tampham.product'
