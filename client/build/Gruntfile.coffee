'use strict'

path = require 'path'

module.exports = (grunt) ->
	grunt.initConfig
		dist: 'D:/TamPham/nginx-1.4.3/tampham'

		coffee:
			all:
				expand: true
				cwd: '../app/'
				src: ['**/*.coffee', '!**/*.spec.coffee']
				dest: '<%= dist %>/app/'
				ext: '.js'

			options:
				bare: true
				sourceMap: true

		mapcat:
			all:
				src: [
					'<%= dist %>/app/**/index.js.map'
					'<%= dist %>/app/**/*.js.map'
				]
				dest: '<%= dist %>/scripts.js'

			options:
				oldRoot: '../app/'
				newRoot: 'src'

		jade:
			all:
				expand: true
				cwd: '../app/'
				src: '**/*.jade'
				dest: '<%= dist %>/app/'
				ext: '.html'

			index:
				src: '../index.jade'
				dest: '<%= dist %>/index.html'

		html2js:
			all:
				src: '<%= dist %>/app/**/*.html'
				dest: '<%= dist %>/templates.js'

			options:
				base: '<%= dist %>'
				module: 'tampham.templates'
				rename: (name) ->
					name.replace /\.html$/, '.jade'
				indentString: '\t'
				quoteChar: "'"

		recess:
			all:
				expand: true
				cwd: '../app/'
				src: '**/*.less'
				dest: '<%= dist %>/app/'
				ext: '.css'

			options:
				compile: true

		stylus:
			all:
				expand: true
				cwd: '../app/'
				src: '**/*.styl'
				dest: '<%= dist %>/app/'
				ext: '.css'

			options:
				compress: false
				urlfunc: 'url'

		cssmin:
			all:
				src: '<%= dist %>/app/**/*.css'
				dest: '<%= dist %>/styles.css'
			options:
				keepBreaks: true

		watch:
			coffee:
				files: ['../app/**/*.coffee', '!../app/**/*.spec.coffee']
				tasks: [
					'coffee'
					'mapcat'
				]
				options:
					spawn: false

			jade:
				files: '../app/**/*.jade'
				tasks: [
					'jade:all'
					'html2js'
				]
				options:
					spawn: false

			index:
				files: '../*.jade'
				tasks: 'jade:index'
				options:
					spawn: false

			recess:
				files: '../app/**/*.less'
				tasks: [
					'recess'
					'cssmin'
				]
				options:
					spawn: false

			stylus:
				files: '../app/**/*.styl'
				tasks: [
					'stylus'
					'cssmin'
				]
				options:
					spawn: false

			options:
				dateFormat: (time) ->
					date = new Date().toLocaleTimeString()
					grunt.log.writeln "Completed in #{time}s at #{date}".cyan
					grunt.log.write 'Waiting... '

	targetFiles = {}

	# Compile only changed files
	grunt.event.on 'watch', (action, filepath, target) ->
		return if target is 'index'
		targetFiles[target] ?= []
		targetFiles[target].push filepath
		onChange()

	onChange = grunt.util._.debounce ->
		for target, files of targetFiles
			grunt.config [target, 'all', 'src'], files
		targetFiles = {}
	, 200

	grunt.loadNpmTasks 'grunt-html2js'
	grunt.loadNpmTasks 'grunt-mapcat'
	grunt.loadNpmTasks 'grunt-recess'

	grunt.loadNpmTasks 'grunt-contrib-coffee'
	grunt.loadNpmTasks 'grunt-contrib-cssmin'
	grunt.loadNpmTasks 'grunt-contrib-jade'
	grunt.loadNpmTasks 'grunt-contrib-stylus'
	grunt.loadNpmTasks 'grunt-contrib-watch'

	grunt.registerTask 'default', [
		'coffee'
		'mapcat'

		'jade'
		'html2js'

		'recess'
		'stylus'
		'cssmin'
	]
